SH          ?= sh

OPAM-URL    := https://github.com/ocaml/opam-repository

PWD         ?= $(shell pwd)
3RD_PARTY   := $(PWD)/3rd_party
CACHE       := $(PWD)/.cache
OPAM2JSON   := $(3RD_PARTY)/opam2json
REPOS       := $(CACHE)/repos
REPOS-OPAM  := $(REPOS)/ocaml/opam-repository
SCRIPTS     := $(PWD)/scripts
SMOKE       := $(CACHE)/smoke
SRC         := $(PWD)/src


.PHONY: all
all: compile


src-%:
	$(MAKE) -C $(SRC) $(*)


.PHONY: clean
clean: src-clean


$(OPAM2JSON)/opam2json:
	$(MAKE) -C $(3RD_PARTY)/opam2json opam2json

.PHONY: compile
compile: $(OPAM2JSON)/opam2json src-compile


.PHONY: install
install: src-install


.PHONY: setup
setup: src-setup


.PHONY: remove
remove: src-remove


$(CACHE):
	mkdir -p $(CACHE)

$(REPOS): $(CACHE)
$(REPOS):
	mkdir -p $(REPOS)

$(REPOS-OPAM): $(REPOS)
$(REPOS-OPAM):
	mkdir -p $(REPOS-OPAM)/../
	git clone $(OPAM-URL) $(REPOS-OPAM)

.PHONY: repos
repos: $(REPOS-OPAM)


.PHONY: test
test: src-test


$(SMOKE):
	mkdir -p $(SMOKE)

smoke-%: $(OPAM2JSON)/opam2json $(REPOS-OPAM)
smoke-%:
	$(SH) $(SCRIPTS)/run.sh --output-directory $(SMOKE) $(*)

.PHONY: smoke
smoke: $(OPAM2JSON)/opam2json $(REPOS-OPAM) $(SMOKE)
smoke: smoke-yojson smoke-alt-ergo
