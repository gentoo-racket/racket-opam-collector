# Racket-Opam-Collector


## About

Generate ebuild scripts form the OCaml's Opam repository


## Setup

Install required Racket packages

```sh
raco pkg install --auto --no-docs --skip-installed \
    ebuild-lib threading-lib upi-lib
```

Compile & get the Opam repository

```sh
make -j $(nproc) compile repos
```
