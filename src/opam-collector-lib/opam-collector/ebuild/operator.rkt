#!/usr/bin/env racket


;; This file is part of racket-opam-collector.

;; racket-opam-collector is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, version 3.

;; racket-opam-collector is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-opam-collector.  If not, see <https://www.gnu.org/licenses/>.

;; Original author: Maciej Barć <xgqt@riseup.net>
;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-only


#lang racket/base

(provide (all-defined-out))


(define (operator? v)
  (and (string? v) (member v '("eq" "geq" "gt" "leq" "lt" "neq"))))

(define (operator->operation operator)
  (case operator
    [("eq") "~"]  ; "~" instead of "=" because of ebuild revisions
    [("geq") ">="]
    [("gt") ">"]
    [("leq") "<="]
    [("lt") "<"]
    [("neq") "!="]
    [else (error 'json->dependency "Unknown operator ~v" operator)]))
