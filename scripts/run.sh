#!/bin/sh


# This file is part of opam-collector.

# opam-collector is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# opam-collector is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with opam-collector.  If not, see <https://www.gnu.org/licenses/>.

# Original author: Maciej Barć <xgqt@riseup.net>
# Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


trap 'exit 128' INT
set -e


myroot="$(dirname "${0}")/../"
project=opam-collector

PATH="${PATH}:${myroot}/3rd_party/opam2json"
export PATH

exec racket "${myroot}/src/${project}-lib/${project}/main.rkt" "${@}"
